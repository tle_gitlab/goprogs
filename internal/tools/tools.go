// +build tools

// Package tools exists to vendor and include tool dependencies:
// https://github.com/golang/go/issues/25922
package tools

import (
	_ "github.com/jstemmer/go-junit-report"
)

func main() {
	panic("this command is not meant to run")
}
