# Go Progressions (GoProgs)

GoProgs are a collection of exercises that progress from beginner level
concepts to intermediate/advanced. The goal with each progression is to
introduce students both to Go language constructs and general computer science
concepts.

All exercises have boilerplate, function stubs, and unit tests.

## Progressions & Problems

- [Algorithms](algorithms) - traditional computer science algorithms
  - Progressions
    - [Binary Search Progression](algorithms/binarysearch.md)
  - Problems
    - linear search
    - binary search
    - ternary search
    - meta binary search
- [Mechanics](mechanics) - entry level Go language constructs
  - Problems
    - [accessing middle of slice](mechanics/middle_index.go)
    - [splitting a slice around element](mechanics/split_slice.go)
    - [recursively split slice](mechanics/recursive_split.go)

## Prerequisites

### Mind Prep

Before you start hacking on this project, make sure you run through the [tour
of Go](https://tour.golang.org/welcome/1). This will give you a quick overview
of the language.

The [Go code review comments
wiki](https://github.com/golang/go/wiki/CodeReviewComments) is a great resource
to learn about common idioms and best practices used by the Go community.

A great reference book to have on hand is
[The Go Programming Language](https://www.gopl.io).

### Software Prep

1. Clone the repo anywhere you want (no `GOPATH` needed).
1. Install `mise` following [this instruction](https://github.com/jdx/mise?tab=readme-ov-file#quickstart).
1. From the current working directory, run `mise install` to install all required tools, include Golang.

## What are progressions?

The progressions are a list of ordered problems to practice solving to gain
enough knowledge to understand a specific concept. Each progression may share
problems with other progressions, this redundancy is intended so that you may
jump around from concept to concept in any order desired.

## How to use progressions

The progressions can be used by individuals or groups wanting to study topics
at their own pace. The intended use is that a moderator will select a
progression and create an issue to discuss that topic. The issue is kept open
as long as there are participants interested in that progression (maybe
forever). Participants will try to work through the progression on their own
time. A video call can be set up to discuss progress/solutions for any opened
progression issues. This way, everyone is able to ask for help no matter where
they are in the progressions.

To begin using the progressions, fork a copy of this repo. Fill in your own
solutions to the problems until your tests pass in CI. Your goal should be to
eventually have all of your tests pass in CI. As new content is made available
in the original repo, pull the content into your forked copy. Rinse and repeat!

### Tips

- The progression will be made up of smaller problems that serve as hints for
  how to solve the bigger problems.

## Contributing

There are many ways to contribute, whether you are learning Go for the first
time or are an already experienced Gopher.

### Contributing Progressions

A progression topic can be suggested by anyone via issue. Once a progression
takes shape, the following guidelines should be followed:

- Make sure the progression has enough exercises to appeal to entry level and
  advanced Gophers.
- Each progression should center on a core problem (e.g. the Binary Search
  progression centers on the binary search algorithm). Deconstruct this core
  problem to create smaller problems as hints to entry level students.
- Add the progression to the list in the [README](README.md).

### Contributing Progression Steps

Students can help shape the progressions by providing feedback on where they
having difficulty in a progression. They also can suggest topics of interest
to inspire the next progression.

If you feel a progression does not have enough detail to guide a student,
please feel free to contribute additional steps. For each new step introduced,
the following are required:

- Two files:
  - A stub file (e.g. `binary_search.go`)
    - should contain a stub for the student to fill out the implementation
    - should contain comments providing background on the problem and any tips
  - A test file (e.g. `binary_search_test.go`)
    - should test the interface defined in the stub
    - should contain at least one test case (preferably more)

The two files should be placed in the appropriate subdirectory (e.g.
`/algorithms`).

## FAQ

- Do I need to do all of the progression steps?
  - No, this is your own practice. If you feel the need to skip around, go
    for it. If you think a problem is too easy, hard or silly, just skip it
    and focus on what you think is important.
